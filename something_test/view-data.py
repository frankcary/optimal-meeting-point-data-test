#!/usr/bin/env python
import pandas as pd
import numpy as np

a = [1,2,3, 4]
amean = np.mean(a)
astd = np.std(a)
acv = astd / amean

b = [100,200,300, 400]
bmean = np.mean(b)
bstd = np.std(b)
bcv = bstd / bmean
print(acv, bcv)

