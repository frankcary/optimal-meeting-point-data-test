#!/usr/bin/env python

# Author: youshaohua
# Desc: this python code execute for generate some original location case0 case1 case2...

import random
import os
import json
import config

def getOneRandOriginLocation():
    randLat = random.randint(config.latitude['min'] * config.zoom, config.latitude['max'] * config.zoom)
    randLog = random.randint(config.longitude['min'] * config.zoom, config.longitude['max'] * config.zoom)
    oneLocatiton = str(randLog/config.zoom) + ',' + str(randLat/config.zoom)
    return oneLocatiton

def getOriginCase():
    case = []
    for i in range(config.originNum):
        case.append(getOneRandOriginLocation())

    # orgin come from config for reading people
    case = config.reading_origin
    return case

def generateData():
    if not os.path.exists(config.casePath):
            os.makedirs(config.casePath)

    for i in range(config.caseNumbers):
        casePath = config.casePath + '/' + config.caseFolder + str(i)
        if not os.path.exists(casePath):
            os.makedirs(casePath)
        oneCase = getOriginCase()
        # write file
        f = open(casePath + '/' + config.originFileName, 'w')
        f.write(json.dumps(oneCase))
        f.close()


# generate Path
generateData()
