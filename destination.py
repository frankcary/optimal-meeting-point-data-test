#!/usr/bin/env python

# Author: youshaohua
# Desc: this python code execute for getting BeiJing all subway sites and generating some test site

from urllib.request import urlopen
import json
import os
import config
import random
import pandas as pd

originalPath = config.casePath + '/' + config.originalDestinationFile
def getRand(cou):
    return random.randint(0, cou - 1)

def getLineNu(lines):
    return getRand(len(lines))

def getRandDestination(json_data):
    destinationCases = []
    for i in range(0,config.destinationNum):
        lineNum = getLineNu(json_data['l'])
        oneLineSt = json_data['l'][lineNum]['st']
        site = oneLineSt[getRand(len(oneLineSt))]
        destinationCases.append(site)
    return destinationCases

def getAllDestination(json_data):
    number = 0
    allDestinations = []
    pair = []
    for line in json_data['l']:
        for site in line['st']:
            if site['sl'] not in pair:
                pair.append(site['sl'])
                allDestinations.append(site)
                number = number + 1
                continue
                if (number < config.destinationNum):
                    allDestinations.append(site)
                    number = number + 1
                else:
                    return allDestinations
    return allDestinations




if not os.path.isfile(originalPath):
    if not os.path.exists(config.casePath):
        os.makedirs(config.casePath)
    response = urlopen(config.BeiJingSubwayUrl)
    data = json.loads(response.read().decode("utf-8"))
    f = open(originalPath, 'w')
    f.write(json.dumps(data))
    f.close()
else:
    json_data=json.loads(open(originalPath).read())
    f = open(config.casePath + '/' + config.tDestinationFileName, 'w')
    # get rand destination site
    #destinations = getRandDestination(json_data)

    # get all destination site
    destinations = getAllDestination(json_data)
    print('destination len:' +  str(len(destinations)))
    f.write(json.dumps(destinations))
    f.close()

