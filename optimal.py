#!/usr/bin/env python

# Author: youshaohua
# Desc: using data-test/case[x]/result.data to get optimal time

import pandas as pd
import config
import os
import numpy as np
import operator

def get_stats(group):
    return {
        #'min': group.min(),
        'max': group.max(),
        'mean': group.mean(),
        'median': group.median(),
        'std': group.std(),
    }

# 获取离散系数
# 离散系数越大表示越离散
def getOneCv(arr):
    tempMean = np.mean(arr.values)
    tempStd = np.std(arr.values)
    return tempStd / tempMean

#Coefficient of Variance
def getAllCv(rt):
    cv = []
    statNames = rt.columns.values
    for key in statNames:
        cv.append({
            'name': key,
            'val': getOneCv(rt[key])
        })
    return cv

def getMaxCv(rt):
    cvs = getAllCv(rt)
    maxCvItem = cvs[0]
    for itemCv in cvs:
        if itemCv['val'] > maxCvItem['val']:
            maxCvItem = itemCv
    return maxCvItem['name']


for x in os.walk(config.casePath):
    filePath = x[0] + '/' + config.resultFileName
    if os.path.isfile(filePath):
        df = pd.read_json(filePath)

        print(df.to_string())
        grouped = df['time'].groupby(df['destination'])
        result = grouped.apply(get_stats).unstack()
        result.columns.name = 'stat'

        tmpCount = result.count()['median']
        halfResult = result
        if tmpCount > 2:
            for i in range(0, tmpCount):
                if tmpCount <= 3:
                    lastResult = halfResult
                    break
                maxCv = getMaxCv(halfResult)
                halfResult = halfResult.sort_values(by=[maxCv])
                tmpCount = int(tmpCount / 2)
                halfResult = halfResult[:tmpCount]
        else:
            lastResult = result

        print(lastResult)
        exit()

