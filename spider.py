#!/usr/bin/env python

# Author: youshaohua
# Desc: this python use amap web api to get original route data

import config
import os
import json
import urllib.request
import random
import threading
import shutil
import re
import time

#read destination data
dLocations = json.loads(open(config.casePath + '/' + config.tDestinationFileName).read())
oneBatchRequestLen = 20

def getOrginAndDestination(pOrigin, dLocations):
    origAndDest = []
    for item in dLocations:
        origAndDest.append('&origin=' + pOrigin +'&destination=' + item['sl'])
    return origAndDest

def getBatchPairs(arrPairs):
    pairsLen = len(arrPairs)
    batchPairs = []
    i = 0
    for pair in arrPairs:
        remainder = i % oneBatchRequestLen
        if (remainder == 0):
            batchPairs.append(arrPairs[i: i + oneBatchRequestLen])
        i = i + 1
    return batchPairs


def requests(oItem, dLocations, tempPath):
    pos = 0
    routeFilePath = tempPath + '/' + oItem + '.data'
    result = []
    pairs = getOrginAndDestination(oItem, dLocations)
    batchPairs = getBatchPairs(pairs)

    for batchItem in batchPairs:
        data = ''
        url = config.batchUrl
        postData = {
            'ops': []
        }



        for oneItem in batchItem:
            postData['ops'].append({
                'url': config.transitUrl + oneItem
            })

        postData = json.dumps(postData).encode('utf-8')
        try:
            time.sleep(random.randint(0, 6))
            stream = urllib.request.urlopen(url, data=postData)
            print(url + '===> end')
        except urllib.error.URLError as e:
            print(e.reason)
        data = json.loads(stream.read().decode("utf-8"))
        result = result + data

    print(routeFilePath + '===>> complated')
    f = open(routeFilePath, 'w', encoding="utf-8")
    f.write(json.dumps(result))
    f.close()


def loadRoute(oLocations, path):
    threads = []

    # create destination route folder
    tempPath = path + '/' + 'routes'
    if os.path.exists(tempPath):
        shutil.rmtree(tempPath)
    os.makedirs(tempPath)

    print('destination len:' + str(len(dLocations)))
    print('origin len:' + str(len(oLocations)))
    for oItem in oLocations:
        threads.append(threading.Thread(target = requests, args=(oItem, dLocations, tempPath,)))

    for item in threads:
        item.start()

    for th in threads:
        th.join()

    print('thread end')



# read origin data
for x in os.walk(config.casePath):
    filePath = x[0] + '/' + config.originFileName
    if os.path.exists(filePath):
        oLocations = json.loads(open(filePath).read())
        loadRoute(oLocations, x[0])


