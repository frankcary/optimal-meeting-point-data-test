#!/usr/bin/env python

# Author: youshaohua
# Desc: this python use original route data to get origin to destination time
# generate data-test/case0/result.data


import config
import pandas as pd
import os
import json
import re


def concatFiles():
    def loadRoute(oLocations, path):
        threads = []

        # create destination route folder
        tempPath = path + '/' + 'routes'

        #contact file
        routeFilePath = path + '/' + config.routeFileName

        # clear routes file
        if os.path.isfile(routeFilePath):
            os.remove(routeFilePath)


        f = open(routeFilePath, 'a')
        pattern = r'data'
        fileNames = os.listdir(tempPath)
        print('route data len:' + str(len(fileNames)))
        for fil in fileNames:
            if len(re.findall(pattern, fil)):
                jsonData = json.loads(open(tempPath + '/' + fil, 'r', encoding="utf-8").read())
                f.write(json.dumps(jsonData) + '\n')
        f.close()



    # read origin data
    for x in os.walk(config.casePath):
        filePath = x[0] + '/' + config.originFileName
        if os.path.exists(filePath):
            oLocations = json.loads(open(filePath).read())
            loadRoute(oLocations, x[0])


def dataHandle():
    QPS_HAS_EXCEEDED_THE_LIMIT = 0
    caseNum = 0
    for x in os.walk(config.casePath):
        routeFileName = x[0] + '/' + config.routeFileName
        resultFileName = x[0] +'/'+ config.resultFileName
        if os.path.isfile(resultFileName):
            os.remove(resultFileName)

        if os.path.isfile(routeFileName):
            pos = 0
            fResult = open(resultFileName, 'a')
            fResult.write('[')

            fr = open(routeFileName, 'r')
            for line in fr:
                json_line = json.loads(line)
                for item in json_line:
                    if not ('route' in item['body']):
                        QPS_HAS_EXCEEDED_THE_LIMIT = QPS_HAS_EXCEEDED_THE_LIMIT + 1
                        continue
                    routes = item['body']['route']
                    origin = routes['origin']
                    destination = routes['destination']
                    if not ('transits' in routes):
                        continue
                    if len(routes['transits']) <= 0:
                        continue
                    if not ('duration' in routes['transits'][0]):
                        continue
                    data = json.dumps({
                        'destination': destination,
                        'origin': origin,
                        'time': routes['transits'][0]['duration']
                    })
                    if pos == 0:
                        fResult.write(data)
                    else:
                        fResult.write(',' + data)
                    pos = pos + 1

            fResult.write(']')
            fResult.close()
            caseNum = caseNum + 1

    print(QPS_HAS_EXCEEDED_THE_LIMIT)
#concat files
concatFiles()

#handle data to get destination , origin and time
dataHandle()

