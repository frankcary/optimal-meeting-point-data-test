#!/usr/bin/env python

# case path
casePath = 'data-test'

#case folder name
caseFolder = 'case'

#case length
caseNumbers = 1

#one case origin numbers
originNum = 6
#origin file name
originFileName = 'origin.data'

#one case destination numbers
destinationNum = 3
#destination file Name
destinationFileName = 'destination.data'
#test destination file Name
tDestinationFileName = 'test-destination.data'

#routes file name
routeFileName = 'routes.data'

zoom = 10000 # for generate orgin case
latitude = {'min': 39.8000, 'max':  40.0800}
longitude = {'min':  116.1924, 'max': 116.6567}

#destination original json data file name
originalDestinationFile = 'original-destination.data'
caseDestinationFile = 'case-destination.data'

#result file name
resultFileName = 'result.data'

# reading people location
reading_origin = [
    '116.3101,39.9076', # 王阳阳     海淀公主坟
    '116.4611,40.0101', # 黄  磊     望馨花园
    '116.5608,39.9118', # 游少华     定福庄南里
    '116.4180,39.9007', # 刘琤&林x溪 崇文门
    '116.3580,39.8896', # 岳慧颖     广安门
    '116.6177,39.9265', # 戴雄杰     像素小区
    '116.4900,39.9671'  # 周书琦     酒仙桥
]

#amap config
key = '118e2e990ed5a3627e0fbcbdb51dd2f1'
amapHost = 'http://restapi.amap.com'
batchUrl = amapHost + '/v3/batch?key=' + key
transitComPara = 'strategy=0&city=北京市&output=json&key=' + key
transitUrl = '/v3/direction/transit/integrated?' + transitComPara
#BeiJing subway data port
BeiJingSubwayUrl = "http://map.amap.com/service/subway?_1480406021461&srhdata=1100_drw_beijing.json"
#address to location
geo = 'http://restapi.amap.com/v3/geocode/geo?key=118e2e990ed5a3627e0fbcbdb51dd2f1&address=北京市 公主坟 地铁站'
